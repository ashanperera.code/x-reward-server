const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const bodyParser = require('body-parser')

// const studentRoutes = require('./routes/student-route')
// const lecturerRoutes = require('./routes/lecturer-route')
// const scheduleRoutes = require('./routes/schedule-route')
// const roleRoutes = require('./routes/role-route')
// const permissionRoutes = require('./routes/permission-route')
// const userRoutes = require('./routes/user-route')
// const authJwtMiddleWare = require('./helpers/auth-token-middleware')
// const errorHanlder = require('./helpers/error-handler-middleware')
const app = express()
require('dotenv/config')

app.use(bodyParser.json())


app.use(morgan('tiny'))
app.use(cors({ origin: { global: true } }))
// app.use(authJwtMiddleWare());
// app.use(errorHanlder.applicationErrorHandler)

const environmentConfigs = process.env
//routes
// app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.studentControllerRoute}`, studentRoutes)
// app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.lecturerControllerRoute}`, lecturerRoutes)
// app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.scheduleControllerRoute}`, scheduleRoutes)
// app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.permissionContrllerRoute}`, permissionRoutes)
// app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.roleControllerRoute}`, roleRoutes)
// app.use(`${environmentConfigs.baseEndpointUrl}${environmentConfigs.apiVersion}${environmentConfigs.userControllerRoute}`, userRoutes)



module.exports = app